<mxfile host="app.diagrams.net" modified="2021-08-19T12:36:16.388Z" agent="5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36" etag="q5MWfh2ZecDt_mXmANCs" version="14.9.6" type="gitlab">
  <diagram id="_2vxG1RFYLvhU1Mq2VF8" name="Page-1">
    <mxGraphModel dx="650" dy="313" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="850" pageHeight="1100" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-1" value="Swati" style="shape=umlActor;verticalLabelPosition=bottom;verticalAlign=top;html=1;outlineConnect=0;" parent="1" vertex="1">
          <mxGeometry x="100" y="210" width="30" height="60" as="geometry" />
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-2" value="" style="shape=parallelogram;perimeter=parallelogramPerimeter;whiteSpace=wrap;html=1;fixedSize=1;" parent="1" vertex="1">
          <mxGeometry x="630" y="180" width="100" height="30" as="geometry" />
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-3" value="laptop" style="rounded=0;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="630" y="210" width="80" height="40" as="geometry" />
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-4" value="" style="shape=flexArrow;endArrow=classic;html=1;" parent="1" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="370" y="240" as="sourcePoint" />
            <mxPoint x="460" y="230" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-5" value="C" style="rounded=0;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="260" y="230" width="60" height="30" as="geometry" />
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-7" value="compiler" style="rounded=0;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="480" y="215" width="60" height="30" as="geometry" />
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-8" value="" style="shape=flexArrow;endArrow=classic;html=1;width=7;endSize=10.07;" parent="1" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="160" y="250" as="sourcePoint" />
            <mxPoint x="230" y="240" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="0VtwrT_qtBvB_Wt2IXa1-9" value="" style="shape=flexArrow;endArrow=classic;html=1;" parent="1" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="550" y="230" as="sourcePoint" />
            <mxPoint x="610" y="220" as="targetPoint" />
            <Array as="points">
              <mxPoint x="560" y="230" />
            </Array>
          </mxGeometry>
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
